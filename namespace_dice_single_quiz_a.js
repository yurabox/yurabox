var namespace_dice_single_quiz_a =
[
    [ "Count", "class_dice_single_quiz_a_1_1_count.html", "class_dice_single_quiz_a_1_1_count" ],
    [ "Dice", "class_dice_single_quiz_a_1_1_dice.html", "class_dice_single_quiz_a_1_1_dice" ],
    [ "Example", "class_dice_single_quiz_a_1_1_example.html", "class_dice_single_quiz_a_1_1_example" ],
    [ "GameManager", "class_dice_single_quiz_a_1_1_game_manager.html", "class_dice_single_quiz_a_1_1_game_manager" ],
    [ "Menu", "class_dice_single_quiz_a_1_1_menu.html", "class_dice_single_quiz_a_1_1_menu" ],
    [ "StudyManager", "class_dice_single_quiz_a_1_1_study_manager.html", "class_dice_single_quiz_a_1_1_study_manager" ]
];