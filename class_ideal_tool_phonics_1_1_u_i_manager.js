var class_ideal_tool_phonics_1_1_u_i_manager =
[
    [ "ColorChange", "class_ideal_tool_phonics_1_1_u_i_manager.html#abedfab6bc05f59b3e271fb47787e9376", null ],
    [ "SelectScaleUp", "class_ideal_tool_phonics_1_1_u_i_manager.html#ae095445c2211753359e0c703ee78f5e5", null ],
    [ "ItemOnOff", "class_ideal_tool_phonics_1_1_u_i_manager.html#a19008ae585683814eb5f94ed5862a2b9", null ],
    [ "alphabetStart", "class_ideal_tool_phonics_1_1_u_i_manager.html#ac343946b45604f235f756b767c021d27", null ],
    [ "Replay", "class_ideal_tool_phonics_1_1_u_i_manager.html#ac3989daaaf4a3c34113c448634cf7203", null ],
    [ "Eraser", "class_ideal_tool_phonics_1_1_u_i_manager.html#a91cfa8eea718042a77ed80c3f1e1f82b", null ],
    [ "Next", "class_ideal_tool_phonics_1_1_u_i_manager.html#a722e4580e15249a58fedfddcee6580e0", null ],
    [ "Prev", "class_ideal_tool_phonics_1_1_u_i_manager.html#ab8772d02e255915ae242c1f26e78bfdb", null ],
    [ "Quit", "class_ideal_tool_phonics_1_1_u_i_manager.html#a638fbb58541ac3b92a1443bc82f27433", null ],
    [ "ButtonSound", "class_ideal_tool_phonics_1_1_u_i_manager.html#a4ac0fbdb795b15603d3cc890ec4d04ed", null ]
];