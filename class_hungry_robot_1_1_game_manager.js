var class_hungry_robot_1_1_game_manager =
[
    [ "CheckAnswer", "class_hungry_robot_1_1_game_manager.html#a029b3ab8a65edbb15b33f1d2aa30bb58", null ],
    [ "PlaySounds", "class_hungry_robot_1_1_game_manager.html#a1582a2a294cfd2f9409499a0593dcc5f", null ],
    [ "PlayExampleSounds", "class_hungry_robot_1_1_game_manager.html#a8a46a4c4cac1705ae9a2a2e6c7611ab9", null ],
    [ "OnClickPreviousButton", "class_hungry_robot_1_1_game_manager.html#a7c06c6249136108f9b7081f7cf87664b", null ],
    [ "OnClickNextButton", "class_hungry_robot_1_1_game_manager.html#a00c554f81d4651ba8cfbfc6a9ccb8a07", null ],
    [ "OnClickRefreshButton", "class_hungry_robot_1_1_game_manager.html#a34c9ab78ec1179bbe400ed09928f2d8a", null ],
    [ "gameState", "class_hungry_robot_1_1_game_manager.html#a9ed67f82c01724994af6815aea45dc1c", null ],
    [ "wordInfo", "class_hungry_robot_1_1_game_manager.html#a5bd5b070426e9b0e7d984164f82f9d30", null ],
    [ "round", "class_hungry_robot_1_1_game_manager.html#a81306f3decd399287f3838633e3695db", null ],
    [ "phonicsInfo", "class_hungry_robot_1_1_game_manager.html#a4904a98f576219f412a11520ee1711d8", null ]
];