var class_sticker =
[
    [ "Trigger", "class_sticker.html#a940d6f17e7a836c9e0d29a6bec457964", null ],
    [ "IsPointerOverUIObject", "class_sticker.html#aa90d33f481c3eba77983961d20d4902d", null ],
    [ "sticker", "class_sticker.html#a091b848ab7c5ad1c1364337ce067683f", null ],
    [ "stickerBackground", "class_sticker.html#ad3c5fe745cf8336d30fb929144e39886", null ],
    [ "stickerDelete", "class_sticker.html#a616fd7299298d900e0e2ce1a98449422", null ],
    [ "stickerRotate", "class_sticker.html#a823bd0885f3edeb1acc8083923450bbc", null ],
    [ "stickerScale", "class_sticker.html#a6b0b3f07610e9ebae7129373ee50f957", null ],
    [ "stickerFlip", "class_sticker.html#adff3ddb8083b413c2dddb76885d121d1", null ],
    [ "fMinSize", "class_sticker.html#a85c988a630b7f4f64c7884b568278122", null ],
    [ "fHalfWidth", "class_sticker.html#a100a1e578b24530efecbedea4bded03f", null ],
    [ "fHalfHeight", "class_sticker.html#a186c18e5f1de0b0b77720bbd06894970", null ],
    [ "newLocation", "class_sticker.html#a6f81bdfc9ac0d907e82bc84fd1bb2094", null ]
];