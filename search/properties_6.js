var searchData=
[
  ['id',['ID',['../class_triangle_net_1_1_data_1_1_triangle.html#add310d735d3730f78d481650f2bf9907',1,'TriangleNet.Data.Triangle.ID()'],['../interface_triangle_net_1_1_geometry_1_1_i_triangle.html#afd40f1aa3bc9bbfbb8bd7cb6d32c4b73',1,'TriangleNet.Geometry.ITriangle.ID()'],['../class_triangle_net_1_1_geometry_1_1_point.html#a2d907963ec3284ff72ac97f5afc83152',1,'TriangleNet.Geometry.Point.ID()'],['../class_triangle_net_1_1_i_o_1_1_input_triangle.html#a241608646f88ef4662240e2519c7db3c',1,'TriangleNet.IO.InputTriangle.ID()'],['../class_triangle_net_1_1_tools_1_1_voronoi_region.html#ab884c735158aca4c011a2c948e84cd53',1,'TriangleNet.Tools.VoronoiRegion.ID()']]],
  ['inputholes',['InputHoles',['../class_triangle_net_1_1_tools_1_1_statistic.html#af75fd859b7f85a06e1b1ae6780f523ed',1,'TriangleNet::Tools::Statistic']]],
  ['inputsegments',['InputSegments',['../class_triangle_net_1_1_tools_1_1_statistic.html#aeb691407fe7deec1c7be9e14902aa124',1,'TriangleNet::Tools::Statistic']]],
  ['inputtriangles',['InputTriangles',['../class_triangle_net_1_1_tools_1_1_statistic.html#acb4953881acaba1a8555a962ef97f3b9',1,'TriangleNet::Tools::Statistic']]],
  ['inputvertices',['InputVertices',['../class_triangle_net_1_1_tools_1_1_statistic.html#af1b3a70c171fbd99fd05aac78d639219',1,'TriangleNet::Tools::Statistic']]],
  ['interiorboundaryedges',['InteriorBoundaryEdges',['../class_triangle_net_1_1_tools_1_1_statistic.html#a44f93ae8f212b715ca120af19529dd96',1,'TriangleNet::Tools::Statistic']]],
  ['ispolygon',['IsPolygon',['../class_triangle_net_1_1_mesh.html#aa666de28a43ed0607ec4a3d5ce558e31',1,'TriangleNet::Mesh']]]
];
