var indexSectionsWithContent =
{
  0: "abcdefghilmnopqrstuvwx",
  1: "abcdefgilmpqrstuvwx",
  2: "cdfhimnostw",
  3: "acdfnops"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions"
};

var indexSectionLabels =
{
  0: "모두",
  1: "데이터 구조",
  2: "네임스페이스들",
  3: "함수"
};

