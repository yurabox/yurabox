var class_ideal_tool_word_1_1_u_i_button =
[
    [ "ColorChange", "class_ideal_tool_word_1_1_u_i_button.html#adbbc8a3879263e5ff0d2b1f31e815733", null ],
    [ "SelectScaleUp", "class_ideal_tool_word_1_1_u_i_button.html#a0363536cfd5e351d242ff3efb4f5ce74", null ],
    [ "ArrowButton", "class_ideal_tool_word_1_1_u_i_button.html#aecc675779937b0ec94d433cd29ecc7e2", null ],
    [ "WordSound", "class_ideal_tool_word_1_1_u_i_button.html#a37d80145146b0506e57054129dae7453", null ],
    [ "Replay", "class_ideal_tool_word_1_1_u_i_button.html#a0e079caca9f98d0efa09bda529788089", null ],
    [ "Eraser", "class_ideal_tool_word_1_1_u_i_button.html#a3b840c8cfaedccddff54a4ea407afe9e", null ],
    [ "Next", "class_ideal_tool_word_1_1_u_i_button.html#a111dae064facbb7cb31f59d0ac77c5b4", null ],
    [ "Prev", "class_ideal_tool_word_1_1_u_i_button.html#aa9b58ed089c065549ccdb608d2a39c43", null ],
    [ "Quit", "class_ideal_tool_word_1_1_u_i_button.html#a8c9666d35e974461016197529f405825", null ],
    [ "ButtonSound", "class_ideal_tool_word_1_1_u_i_button.html#a30db4a242d59aefa66324420ebc0410f", null ]
];