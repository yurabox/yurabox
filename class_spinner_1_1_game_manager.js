var class_spinner_1_1_game_manager =
[
    [ "spinnerObject", "class_spinner_1_1_game_manager.html#a526af66d6148b69e6d1487dda45a4801", null ],
    [ "spinnerCenterDefaultImage", "class_spinner_1_1_game_manager.html#a90c13e885072e34bf740befcd1098101", null ],
    [ "totalRound", "class_spinner_1_1_game_manager.html#ac3b4188530925733cff0b2c5f1745c61", null ],
    [ "answerWords", "class_spinner_1_1_game_manager.html#ad32bdc67c8d1d1993629adc8887eb748", null ],
    [ "examplesWords", "class_spinner_1_1_game_manager.html#ab448e55841fb78282b97e735a9605853", null ],
    [ "examplesUpperWords", "class_spinner_1_1_game_manager.html#a8e389160f36301e4fe6d4b3b3a48afff", null ],
    [ "examplesLowerWords", "class_spinner_1_1_game_manager.html#a40939c35e300f2650eb3b4e5ee4018cb", null ],
    [ "exampleImages", "class_spinner_1_1_game_manager.html#aa00c74ab07b2e6b6d145d972c3668662", null ],
    [ "spinnerCenterImage", "class_spinner_1_1_game_manager.html#aea6787dae5f9a0aae7e1d6db56a8a69a", null ],
    [ "gameState", "class_spinner_1_1_game_manager.html#a0f8123d04612722138ef943e7a9219b7", null ],
    [ "curruntRound", "class_spinner_1_1_game_manager.html#a06cdb170ced7754edb27129b08991fc9", null ],
    [ "CurrentRound", "class_spinner_1_1_game_manager.html#a9ed73edce31cfff2db97c768d0714cd3", null ]
];