var class_hungry_robot_1_1_robot_controler =
[
    [ "InitGame", "class_hungry_robot_1_1_robot_controler.html#a894dbf630f1761ede09df8c905f97715", null ],
    [ "BeginDrag", "class_hungry_robot_1_1_robot_controler.html#a23e760be2cf5a9bc3402b18a1bbda0a4", null ],
    [ "EndDrag", "class_hungry_robot_1_1_robot_controler.html#a1f9d41231f46f55e9ec52acfe83a96a2", null ],
    [ "CorrectAnswer", "class_hungry_robot_1_1_robot_controler.html#a8a728b217b3fb79b563c74db412718be", null ],
    [ "WrongAnswer", "class_hungry_robot_1_1_robot_controler.html#a762f48ad4a337e93a63b4a5fb4223aa4", null ],
    [ "EndGame", "class_hungry_robot_1_1_robot_controler.html#aa7dfa324079194014191d3f6b91b0115", null ],
    [ "GoIdle", "class_hungry_robot_1_1_robot_controler.html#ae979ea69cd16132c66b225dd9e8811d5", null ],
    [ "robotMouth", "class_hungry_robot_1_1_robot_controler.html#a5936ab969d7c8d500efbdf720fcac052", null ]
];