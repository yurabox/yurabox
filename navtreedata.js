/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "KidsBrown IR AllGames", "index.html", [
    [ "패키지", "namespaces.html", [
      [ "패키지", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "데이터 구조", "annotated.html", [
      [ "데이터 구조", "annotated.html", "annotated_dup" ],
      [ "데이터 구조 색인", "classes.html", null ],
      [ "클래스 계통도", "hierarchy.html", "hierarchy" ],
      [ "데이터 필드", "functions.html", [
        [ "모두", "functions.html", null ],
        [ "함수", "functions_func.html", null ]
      ] ]
    ] ],
    [ "파일들", "files.html", [
      [ "파일 목록", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"01_8_01_flash_01_card_01_basic_202_8_01_scripts_201_8_01_game_2_game_manager_8cs_source.html",
"class_fill_in_the_blanks_1_1_main_image_item.html#a58500fa914d3106a147eb971bbca86f1",
"class_ideal_tool_phonics_1_1_u_i_manager.html",
"class_word_scramble_1_1_word_list_item.html"
];

var SYNCONMSG = '패널 동기화를 비활성화하기 위해 클릭하십시오';
var SYNCOFFMSG = '패널 동기화를 활성화하기 위해 클릭하십시오';