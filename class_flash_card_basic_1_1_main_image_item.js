var class_flash_card_basic_1_1_main_image_item =
[
    [ "SetInfo", "class_flash_card_basic_1_1_main_image_item.html#a57363ca6488323cd7b6e60146d8b05b2", null ],
    [ "OnClickMainItem", "class_flash_card_basic_1_1_main_image_item.html#a86ccd4fb9ed1db55f74401059c63c438", null ],
    [ "RotateImage", "class_flash_card_basic_1_1_main_image_item.html#ad2b0f52b173c3c25daf656a0c7fe43e1", null ],
    [ "RotateText", "class_flash_card_basic_1_1_main_image_item.html#ad5605e15ea87a00f84d34e0eea4ae1f3", null ],
    [ "RotateImmediately", "class_flash_card_basic_1_1_main_image_item.html#ad3967d941d3f85182fcb218e83aca1a2", null ],
    [ "isFront", "class_flash_card_basic_1_1_main_image_item.html#a6df20ddecd2bc3ff765aa1821d65d6f4", null ]
];