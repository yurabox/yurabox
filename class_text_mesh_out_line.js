var class_text_mesh_out_line =
[
    [ "dead", "class_text_mesh_out_line.html#a2f5c99cb56a50e16be192b7093a2a4c3", null ],
    [ "pixelSize", "class_text_mesh_out_line.html#a15d8e7632699a0d84b9f7d4fe074a3ae", null ],
    [ "outlineColor", "class_text_mesh_out_line.html#aa29d4d88c7f5247dde0a450f531ffd8f", null ],
    [ "resolutionDependant", "class_text_mesh_out_line.html#aebb3326501f04a4e5aed01cd956c623f", null ],
    [ "doubleResolution", "class_text_mesh_out_line.html#a14c63d4f4f752311ddfbb28c68fc5781", null ],
    [ "sortingOrder", "class_text_mesh_out_line.html#a61054f85917da4afbe5519fe4d27ebe5", null ]
];